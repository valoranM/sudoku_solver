#ifndef SETUP_H
#define SETUP_H

#include "sudoku.h"

extern void parse_file(const char *filename, sudoku_t *sudoku);

#endif /* SETUP_H */