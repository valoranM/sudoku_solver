#include <stdlib.h>
#include <stdio.h>

#include "stack.h"

stack_t *add_box(int *box, stack_t *stack)
{
    stack_t *new = (stack_t *)malloc(sizeof(stack_t));
    new->box = box;
    new->next = stack;
    new->choices = NULL;
    return new;
}

void add_choice(int choice, stack_t *stack)
{
    choices_t *new = (choices_t *)malloc(sizeof(choices_t));
    new->choice = choice;
    new->next = stack->choices;
    stack->choices = new;
}

void next_choice(stack_t *stack)
{
    choices_t *tmp = stack->choices;
    stack->choices = tmp->next;
    if (stack->choices != NULL)
    {
        *(stack->box) = stack->choices->choice;
    }
    free(tmp);
}

static void clean_choices(stack_t *stack);

stack_t *pop_box(stack_t *stack)
{
    stack_t *tmp = stack->next;
    clean_choices(stack);
    *(stack->box) = 0;
    free(stack);
    return tmp;
}

void clean_stack(stack_t *stack)
{
    stack_t *tmp;
    while (stack != 0)
    {
        clean_choices(stack);
        tmp = stack;
        stack = tmp->next;
        free(tmp);
    }
}

static void clean_choices(stack_t *stack)
{
    choices_t *tmp;
    while (stack->choices != 0)
    {
        tmp = stack->choices;
        stack->choices = tmp->next;
        free(tmp);
    }
}