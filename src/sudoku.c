#include <stdlib.h>
#include <stdio.h>

#include "sudoku.h"
#include "setup.h"
#include "utils.h"
#include "stack.h"

static int *numbers_used;
static int *numbers_available;
static int number_of_zero;

static void neigbours_number(int x, int y, const sudoku_t sudoku);
static int set_available_numbers(const sudoku_t sudoku);
static int verify_sudoku(const sudoku_t sudoku);
static int verify_sudoku(const sudoku_t sudoku);

void init_sudoku(const char *filename, sudoku_t *sudoku)
{
    parse_file(filename, sudoku);
    numbers_used = (int *)malloc(sizeof(int) * (sudoku->number_of_number + 1));
    numbers_available = (int *)malloc(sizeof(int) * (sudoku->number_of_number + 1));
    number_of_zero = 0;
    for (int i = 0; i < sudoku->number_of_number; i++)
    {
        numbers_used[i] = 0;
        numbers_available[i] = 0;
    }
    if (verify_sudoku(*sudoku))
    {
        print_sudoku(*sudoku);
        error("invalid sudoku");
    }
}

int get_number(int x, int y, const sudoku_t sudoku)
{
    return sudoku.grid[y * sudoku.number_of_number + x];
}

static void neigbours_number(int x, int y, const sudoku_t sudoku)
{
    for (int i = 0; i <= sudoku.number_of_number; i++)
    {
        numbers_used[i] = 0;
        numbers_available[i] = 1;
    }
    for (int i = 0; i < sudoku.number_of_number; i++)
    {
        numbers_used[get_number(x, i, sudoku)]++;
        numbers_used[get_number(i, y, sudoku)]++;
        int case_x = x - (x % sudoku.case_col) + (i % sudoku.case_col),
            case_y = y - (y % sudoku.case_line) + ((i / sudoku.case_col) % sudoku.case_line);
        numbers_used[get_number(case_x, case_y, sudoku)]++;
    }
}

static int set_available_numbers(const sudoku_t sudoku)
{
    int count = 0;
    for (size_t i = 1; i < sudoku.number_of_number + 1; i++)
    {
        numbers_available[i] = !numbers_used[i];
        if (!numbers_used[i])
        {
            count++;
        }
    }
    return count;
}

static int verify_sudoku(const sudoku_t sudoku)
{
    for (size_t y = 0; y < sudoku.number_of_number; y++)
    {
        for (size_t x = 0; x < sudoku.number_of_number; x++)
        {
            neigbours_number(x, y, sudoku);
            int possibility = set_available_numbers(sudoku);
            if (get_number(x, y, sudoku) == 0)
            {
                number_of_zero++;
            }
            if ((get_number(x, y, sudoku) == 0 && possibility == 0) ||
                (get_number(x, y, sudoku) != 0 && numbers_used[get_number(x, y, sudoku)] != 3))
            {
                fprintf(stderr, "(%d, %d) %d\n", x, y, get_number(x, y, sudoku));
                return 1;
            }
        }
    }
    return 0;
}

static void change_min(const sudoku_t sudoku);
static int *find_min(const sudoku_t sudoku);
static void report_min_choices(const sudoku_t sudoku);
static void backtrack(const sudoku_t sudoku);

int *min_choices;
int count;

stack_t *stack;

void sudoku_solving(sudoku_t sudoku)
{
    min_choices = (int *)malloc(sizeof(int) * sudoku.number_of_number);
    stack = NULL;
    while (number_of_zero > 0)
    {
        change_min(sudoku);
    }
    free(min_choices);
}

static void change_min(const sudoku_t sudoku)
{
    int *min = find_min(sudoku);
    if (count == 0)
    {
        backtrack(sudoku);
    }
    else
    {
        stack = add_box(min, stack);
        for (size_t i = 0; i < count; i++)
        {
            add_choice(min_choices[i], stack);
        }
        *min = stack->choices->choice;
        number_of_zero--;
    }
}

static void backtrack(const sudoku_t sudoku)
{
    next_choice(stack);
    while (stack->choices == 0)
    {
        if (stack->choices == NULL)
        {
            stack = pop_box(stack);
            number_of_zero++;
            if (stack == NULL)
            {
                error("unsolvable sudoku");
            }
        }
        next_choice(stack);
    }
}

static int *find_min(const sudoku_t sudoku)
{
    int choices_min = sudoku.number_of_number;
    int *min = NULL;
    for (size_t y = 0; y < sudoku.number_of_number; y++)
    {
        for (size_t x = 0; x < sudoku.number_of_number; x++)
        {
            if (get_number(x, y, sudoku) == 0)
            {
                neigbours_number(x, y, sudoku);
                int min_tmp = set_available_numbers(sudoku);
                if (min_tmp < choices_min)
                {
                    min = &(sudoku.grid[y * sudoku.number_of_number + x]);
                    report_min_choices(sudoku);
                    choices_min = min_tmp;
                }
            }
        }
    }
    return min;
}

static void report_min_choices(const sudoku_t sudoku)
{
    count = 0;
    for (size_t i = 0; i < sudoku.number_of_number; i++)
    {
        if (numbers_available[i + 1] > 0)
        {
            min_choices[count] = i + 1;
            count++;
        }
    }
}

void clean_sudoku(sudoku_t *sudoku)
{
    free(sudoku->grid);
    free(numbers_available);
    free(numbers_used);
    clean_stack(stack);
}