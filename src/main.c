#include <stdio.h>
#include <time.h>

#include "setup.h"
#include "sudoku.h"
#include "utils.h"

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "./sudoku_solver <filename>\n");
        return 1;
    }
    else
    {
        sudoku_t sudoku;
        init_sudoku(argv[1], &sudoku);
        print_sudoku(sudoku);

        time_t begin = clock();
        sudoku_solving(sudoku);
        time_t end = clock();

        printf("\nSOLVING\n-------\n");
        print_sudoku(sudoku);
        printf("%fs\n", ((double)end - begin) / CLOCKS_PER_SEC);
        clean_sudoku(&sudoku);
        return 0;
    }
}
