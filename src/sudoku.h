#ifndef SUDOKU_H
#define SUDOKU_H

typedef struct suoku_s
{
    int case_col, case_line;
    int number_of_number;
    int *grid;
} sudoku_t;

extern void init_sudoku(const char *filename, sudoku_t *sudoku);
extern int get_number(int x, int y, const sudoku_t sudoku);
extern void sudoku_solving(sudoku_t sudoku);
extern void clean_sudoku(sudoku_t *sudoku);

#endif /* SUDOKU_H */