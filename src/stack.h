#ifndef STACK_H
#define STACK_H

typedef struct choices_s
{
    int choice;
    struct choices_s *next;
} choices_t;

typedef struct stack_s
{
    int *box;
    choices_t *choices;
    struct stack_s *next;
} stack_t;

extern stack_t *add_box(int *box, stack_t *stack);
extern void add_choice(int choice, stack_t *stack);
extern void next_choice(stack_t *stack);
extern stack_t *pop_box(stack_t *stack);
extern void clean_stack(stack_t *stack);

#endif /* STACK_H */