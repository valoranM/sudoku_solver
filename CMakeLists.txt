cmake_minimum_required(VERSION 3.0)

project(sudoku_solver VERSION 0.0.1)

find_package(SDL2 REQUIRED)

set(SRCS
    src/setup.c
    src/stack.c
    src/sudoku.c
    src/utils.c
)

set(HEADERS
    src/setup.h
    src/stack.h
    src/sudoku.h
    src/utils.h
)

set(LIBS
    m
)

add_executable(sudoku_solver src/main.c ${SRCS} ${HEADERS})
target_compile_features(sudoku_solver PRIVATE cxx_std_17)
target_include_directories(sudoku_solver PUBLIC src/)
target_link_libraries(sudoku_solver ${LIBS})
